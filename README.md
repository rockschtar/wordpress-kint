# WordPress Kint #

Integriert PHP-Kint (http://raveren.github.io/kint/) in WordPress. 

### What is it? ###

Kint for PHP is a tool designed to present your debugging data in the absolutely best way possible.

In other words, it's var_dump() and debug_backtrace() on steroids. Easy to use, but powerful and customizable. An essential addition to your development toolbox.