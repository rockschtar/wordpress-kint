<?php

/*
  Plugin Name: WordPress Kint
  Plugin URI: http://www.validio.de
  Description: Kint Support for Wordpress Developers
  Version: 1.0
  Author: Stefan Helmer
  Author URI: http://www.validio.de
 */

define("WPKINT_PLUGIN_DIR", plugin_dir_path(__FILE__));
define("WPKINT_PLUGIN_URL", plugin_dir_url(__FILE__));
define("WPKINT_PLUGIN_FILE", __FILE__);
define("WPKINT_PLUGIN_RELATIVE_DIR", dirname(plugin_basename(__FILE__)));

if (!class_exists("Kint")) {
    require WPKINT_PLUGIN_DIR . '/kint-0.9/Kint.class.php';
}

add_action('activated_plugin', 'wpkint_load_first');

function wpkint_load_first() {
    $path = str_replace(WP_PLUGIN_DIR . '/', '', __FILE__);
    if ($plugins = get_option('active_plugins')) {
        if ($key = array_search($path, $plugins)) {
            array_splice($plugins, $key, 1);
            array_unshift($plugins, $path);
            update_option('active_plugins', $plugins);
        }
    }
}